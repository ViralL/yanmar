'use strict';

// ready
$(document).ready(function () {

    // anchor
    $(".anchor").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top }, 1000);
    });
    // anchor

    // mask phone {maskedinput}
    $("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // animation
    $('.animated').appear(function () {
        var elem = $(this);
        var animation = elem.data('animation');

        if (!elem.hasClass('visible')) {
            var animationDelay = elem.data('animation-delay');
            if (animationDelay) {
                setTimeout(function () {
                    elem.addClass(animation + " visible");
                }, animationDelay);
            } else {
                elem.addClass(animation + " visible");
            }
        }
    });
    // animation

    // popup {magnific-popup}
    //$('.image-gallery').magnificPopup({});
    // popup
});
// ready
//# sourceMappingURL=main.js.map
